package com.maxsch.gitstub.api;

import com.maxsch.gitstub.data.Repository;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import java.util.List;

public interface GitApi {
    @GET("/repositories")
    Call<List<Repository>> repositories(@Query("since") long lastRepoID);
}
