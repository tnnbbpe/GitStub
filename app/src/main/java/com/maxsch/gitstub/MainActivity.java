package com.maxsch.gitstub;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;
import com.maxsch.gitstub.data.Repository;
import com.maxsch.gitstub.data.RepositoryAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    RecyclerView recyclerView;
    private static final String TAG = "MainActivity";
    private long repCount;
    private Long lastRepoId;
    public static String url;
    List<Repository> repositories;
    LinearLayoutManager layoutManager = new LinearLayoutManager(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final SwipeRefreshLayout swipeRefreshLayout = findViewById(R.id.swipeContainer);

        repositories = new ArrayList<>();

        final RepositoryAdapter adapter = new RepositoryAdapter(repositories);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        getData(0);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                repCount = RepositoryAdapter.totalRepoCount + 10;
                if(repCount == repositories.size()) {
                    try {
                        lastRepoId = Long.valueOf(repositories.get(repositories.size()-1).getId());
                    } catch (NumberFormatException e) {
                        Log.e(TAG,"Unable to convert ID to Long",e);
                    }
                    getData(lastRepoId);
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                repositories.clear();
                getData(0);
                recyclerView.getAdapter().notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    void getData(long repos) {
        App.getApi().repositories(repos).enqueue(new Callback<List<Repository>>() {
            @Override
            public void onResponse(@NonNull Call<List<Repository>> call, @NonNull Response<List<Repository>> response) {
                repositories.addAll(response.body());
                Log.i(TAG, "" + repositories.size());
                recyclerView.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void onFailure(@NonNull Call<List<Repository>> call, @NonNull Throwable t) {
                Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }

        });

    }
    public void start(){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, url);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, "Send link"));
    }
}
