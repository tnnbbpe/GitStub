package com.maxsch.gitstub.view;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.maxsch.gitstub.MainActivity;
import com.maxsch.gitstub.R;

public class ViewHolder extends RecyclerView.ViewHolder {
    public TextView Name;
    public TextView Id;
    public TextView Desc;

    public ViewHolder(View itemView) {
        super(itemView);

        Name = itemView.findViewById(R.id.repository_name);
        Id = itemView.findViewById(R.id.repository_id);
        Desc = itemView.findViewById(R.id.repository_description);
    }
}
