package com.maxsch.gitstub.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

class Owner {

    @SerializedName("login")
    @Expose
    private String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}