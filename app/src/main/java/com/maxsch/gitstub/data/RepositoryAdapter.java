package com.maxsch.gitstub.data;

import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.maxsch.gitstub.MainActivity;
import com.maxsch.gitstub.R;
import com.maxsch.gitstub.data.Repository;
import com.maxsch.gitstub.view.ViewHolder;
import retrofit2.Response;

import java.util.List;

public class RepositoryAdapter extends RecyclerView.Adapter<ViewHolder> {

    private List<Repository> repositories;
    public static int totalRepoCount = 0;

    public RepositoryAdapter(List<Repository> repositories) {
        this.repositories = repositories;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        totalRepoCount = position;
        Repository repository = repositories.get(position);
        holder.Desc.setText(repository.getDescription());
        holder.Id.setText(repository.getId());
        holder.Name.setText(repository.getName() + "/" + repository.getOwner().getLogin());
    }

    @Override
    public int getItemCount() {
        if (repositories == null)
            return 0;
        else return repositories.size();
    }
}
