package com.maxsch.gitstub;

import android.app.Application;
import android.util.Log;
import com.maxsch.gitstub.api.GitApi;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class App extends Application{

    private static final String BASE_URL = "https://api.github.com/";
    private static GitApi gitApi;
    private Retrofit retrofit;

    @Override
    public void onCreate() {
        super.onCreate();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        gitApi = retrofit.create(GitApi.class);
    }

    public static GitApi getApi(){
        return gitApi;
    }



}